﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InruleUnitTest.InruleApplicationService
{
    public interface IInruleExcutionService
    {
        TestSuiteResponse ExcuteTestSuite(TestSuiteRequest request);

        IEnumerable<TestSuiteResponse> ExcuteTestSuites(IList<TestSuiteRequest> requests);
    }
}
