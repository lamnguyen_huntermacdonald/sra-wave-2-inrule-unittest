﻿using InruleUnitTest.Domain;

namespace InruleUnitTest.InruleApplicationService
{
    public class TestSuiteRequest
    {
        public TestSuiteFile TestSuite { get; set; }
    }
}