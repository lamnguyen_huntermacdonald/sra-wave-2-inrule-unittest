﻿using InRule.Runtime.Testing.Regression;

namespace InruleUnitTest.InruleApplicationService
{
    public class TestSuiteResponse
    {
        public TestResultCollection Result { get; set; }
        public string ErrorText { get; set; }
    }
}