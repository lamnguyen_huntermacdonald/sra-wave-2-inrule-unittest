﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InRule.Runtime.Testing.Regression;
using InruleUnitTest.Repository;

namespace InruleUnitTest.InruleApplicationService
{
    public class InruleExcutionService : IInruleExcutionService
    {
        private readonly IInRuleExcutionRepository _inruleRepository;

        public InruleExcutionService(IInRuleExcutionRepository inruleRepository)
        {
            this._inruleRepository = inruleRepository;
        }

        public TestSuiteResponse ExcuteTestSuite(TestSuiteRequest request)
        {
            var result = new TestSuiteResponse();
            try
            {
                result.Result = _inruleRepository.ExcuteTestSuite(request.TestSuite);
            }
            catch (Exception ex)
            {
                result.ErrorText = ex.Message;
            }

            return result;
        }

        public IEnumerable<TestSuiteResponse> ExcuteTestSuites(IList<TestSuiteRequest> requests)
        {            
            foreach(var request in requests)
            {
                yield return ExcuteTestSuite(request);
            }
           
        }
    }
}
