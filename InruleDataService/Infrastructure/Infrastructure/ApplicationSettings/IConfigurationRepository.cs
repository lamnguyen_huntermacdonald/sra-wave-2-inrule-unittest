﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InruleUnitTest.Infrastructure.Common
{
	public interface IConfigurationRepository
	{
		T GetConfigurationValue<T>(string key);
		T GetConfigurationValue<T>(string key, T defaultValue);
	}
}
