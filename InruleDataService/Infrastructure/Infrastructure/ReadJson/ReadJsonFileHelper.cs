﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.Configuration;

namespace InruleUnitTest.Infrastructure.Common
{
    public static class ReadJsonFileHelper
    {
        
        public static string Description(this Enum value)
        {
            // variables  
            var enumType = value.GetType();
            var field = enumType.GetField(value.ToString());
            var attributes = field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            string result = value.ToString();
            foreach (var item in attributes)
                result = ((DescriptionAttribute)item).Description;
            return result;
        }

        public static byte[] StreamToByteArray(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        public static byte[] ReadAllBytes(string fileName)
        {
            byte[] buffer = null;
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                buffer = new byte[fs.Length];
                fs.Read(buffer, 0, (int)fs.Length);
            }
            return buffer;
        }
        public static string DictToString(IDictionary<string, object> dict)
        {
            StringBuilder builder = new StringBuilder();

            string appendedText = string.Empty;
            foreach (KeyValuePair<string, object> kvp in dict)
            {
                appendedText = string.Empty;

                if (kvp.Value is JObject)
                {
                    int index = 0;

                    var jObject = kvp.Value as JObject;
                    foreach (var jProperty in jObject.Properties())
                    {
                        string indexedPropertyName = $"{kvp.Key}[{index++}]";
                        appendedText = $"{indexedPropertyName}.key={jProperty.Name}&{indexedPropertyName}.value={jProperty.Value.ToString()}&";
                    }
                }
                else    // kvp.Value is 'flat' object
                {
                    appendedText = kvp.Key + "=" + kvp.Value + "&";
                }

                if (!string.IsNullOrWhiteSpace(appendedText))
                    builder.Append(appendedText);
            }

            if (builder.Length > 0) builder.Remove(builder.Length - 1, 1);
            return builder.ToString();
        }

        //public static  string DictToString(NameValueCollection collection)
        //{
        //    StringBuilder builder = new StringBuilder();
        //    foreach (string key in collection.AllKeys)
        //        builder.Append(key + "=" + collection[key] + "&");

        //    if (builder.Length > 0) builder.Remove(builder.Length - 1, 1);
        //    return builder.ToString();
        //}

        public static Dictionary<string, object> ObjectToDictionary(object data)
        {
            var dic = (from x in data.GetType().GetProperties() select x)
                        .ToDictionary(x => x.Name, x => x.GetGetMethod().Invoke(data, null));
            return dic;
        }

        public static string UrlParamCombine(string url, string paramsString)
        {
            if (url.Contains("?"))
                url = string.Concat(url, "&", paramsString);
            else
                url = string.Concat(url, "?", paramsString);
            return url;
        }

        public static string UrlParamCombine(string url, Dictionary<string, object> urlparams)
        {
            return UrlParamCombine(url, DictToString(urlparams));
        }

        public static string UrlParamCombine(string url, object data, IEnumerable<PropertyInfo> properties = null)
        {
            return UrlParamCombine(url, UrlQueryCombine(data, properties));
        }

        public static string UrlQueryCombine(object data, IEnumerable<PropertyInfo> properties = null)
        {
            if (properties == null)
                if (data == null)
                    return string.Empty;
                else
                    properties = data.GetType().GetProperties();

            StringBuilder result = new StringBuilder();
            foreach (var p in properties)
            {
                var value = p.GetValue(data);
                string textValue = null;
                if (value != null)
                {
                    var pti = p.PropertyType.GetTypeInfo();
                    if (pti.IsArray || (pti.IsGenericType && Nullable.GetUnderlyingType(p.PropertyType) == null))
                    {
                        var arrValue = new StringBuilder();
                        foreach (var subItem in (IEnumerable)value)
                        {
                            arrValue.Append($"{subItem},");
                        }
                        textValue = WebUtility.UrlEncode(arrValue.ToString().PadRight(1));
                    }
                    else
                        textValue = WebUtility.UrlEncode(value.ToString());
                }
                else
                    textValue = string.Empty;

                result.Append($"{p.Name}={WebUtility.UrlEncode(textValue)}&");
            }
            return result.Remove(result.Length - 1, 1).ToString();
        }

        public static string UrlPathCombine(string left, params object[] right)
        {
            if (right.Length == 0 || string.IsNullOrWhiteSpace(left) || string.IsNullOrWhiteSpace(right[0].ToString()))
                return left;

            string curRight = right[0].ToString();

            // Add "/" to last char of "left"
            if (left.LastIndexOf("/") != left.Length - 1)
                left += "/";

            // Remove "/" of first char of right
            if (curRight.IndexOf("/") == 0)
                curRight = curRight.Substring(1);

            return UrlPathCombine(string.Concat(left, curRight), right.Skip(1).ToArray());
        }

        public static string CursiveExceptionMessage(Exception ex)
        {
            string message = ex.Message + " --------------------- " + ex.StackTrace;
            while (ex.InnerException != null)
            {
                message += "\r\n\n" + ex.InnerException.Message + " --------------------- " + ex.InnerException.StackTrace;
                ex = ex.InnerException;
            }
            return message;
        }

        public static JToken Rename(JToken json, Dictionary<string, string> map)
        {
            return Rename(json, name => map.ContainsKey(name) ? map[name] : name);
        }

        public static JToken Rename(JToken json, Func<string, string> map)
        {
            JProperty prop = json as JProperty;
            if (prop != null)
            {
                return new JProperty(map(prop.Name), Rename(prop.Value, map));
            }

            JArray arr = json as JArray;
            if (arr != null)
            {
                var cont = arr.Select(el => Rename(el, map));
                return new JArray(cont);
            }

            JObject o = json as JObject;
            if (o != null)
            {
                var cont = o.Properties().Select(el => Rename(el, map));
                return new JObject(cont);
            }

            return json;
        }

        public static byte[] ReadStreamToEnd(System.IO.Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }

        public static string ConvertJObjectToDate(JToken jdate, string format = "dd/MM/yyyy")
        {
            if (jdate == null) return string.Empty;
            DateTime dateTime = DateTime.MinValue;
            DateTime.TryParse(jdate.ToString(), out dateTime);
            if (dateTime == DateTime.MinValue)
                return string.Empty;
            else
                return dateTime.ToString(format);
        }

        // Convert an object to a byte array
        public static byte[] ObjectToByteArray(Object obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        // Convert a byte array to an Object
        public static Object ByteArrayToObject(byte[] arrBytes)
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                var obj = binForm.Deserialize(memStream);
                return obj;
            }
        }

        
        public static string GetDataFromJson(string fileName,  string path) {

            IConfigurationRepository congig = new ConfigFileConfigurationRepository();
            try
            {
                fileName = $@"{congig.GetConfigurationValue<string>("FolderPathFileMockData")}\{fileName}.json";
                if (string.IsNullOrEmpty(fileName) ||  !File.Exists(fileName))
                    return "";              


                string jsondata = File.ReadAllText(fileName);

                var arrjt = JsonHelper.GetValueFromJsonPath(jsondata, path);
                
                return arrjt.ToString();
            }
            catch (Exception ex)
            {

            }
            return "";
        }

        public static IDictionary<string,string> JsonToDictionary(string fileName, string path)
        {
            JToken  jsonToken = GetDataFromJson(fileName, path);
            IDictionary<string, string> result = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(jsonToken.ToString()))
            {
                JArray array = JArray.Parse(jsonToken.ToString());
                foreach (JObject content in array.Children<JObject>())
                {
                    foreach (JProperty prop in content.Properties())
                    {
                        result.Add(prop.Name, prop.Value.ToString());
                    }
                }
            }
            
            return result;

        }
    }
}