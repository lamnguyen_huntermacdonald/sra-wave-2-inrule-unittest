﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Xml;

namespace InruleUnitTest.Infrastructure.Common
{
    public static class JsonHelper
    {
        public static JToken GetValueFromJsonPath(string jsondata, string jsonpath)
        {

            JToken jsonTokenreturn = null;
            try
            {
                JObject jobject = JObject.Parse(jsondata);
                jsonTokenreturn = jobject.SelectToken(jsonpath);


            }
            catch {

            }
            return jsonTokenreturn;
        }
        public static JToken GetValueFromJsonPathAndPathFile(string jsonfilepath, string jsonpath)
        {
            string jsondata = "";
            if (File.Exists(jsonfilepath))
            {
                jsondata = File.ReadAllText(jsonfilepath);
                return GetValueFromJsonPath(jsondata, jsonpath);
            }
            else
                return null;
          
        }

        public static string SerializeObject(object value)
        {
            try
            {
                return JsonConvert.SerializeObject(value);
            }
            catch
            {
                return "";
            }
        }

        public static string JsonToXml(string json)
        {
            XmlDocument doc = JsonConvert.DeserializeXmlNode(json);
            return doc.InnerXml;
        }

        public static string XmlToJson(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            var json = JsonConvert.SerializeXmlNode(doc);
            return json.ToString();
        }

        public static T DeserializeObject<T>(string value)
        {
            try
            {
                return (T)JsonConvert.DeserializeObject<T>(value);
            }
            catch {
            }
            return default(T);
        }

    }
}
