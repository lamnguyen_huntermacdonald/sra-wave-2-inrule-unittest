﻿using InRule.Repository;
using InRule.Repository.Client;
using InRule.Repository.Regression;
using InRule.Runtime.Testing.Regression;
using InRule.Runtime.Testing.Regression.Runtime;
using InRule.Runtime.Testing.Session;
using InRuleDataService;
using InRuleDataService.Helpers;
using InruleUnitTest.Domain;
using InruleUnitTest.Infrastructure.Common;
using Microsoft.Win32.SafeHandles;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace InruleUnitTest.Repository
{
    public class InRuleExcutionRepository: IInRuleExcutionRepository, IDisposable 
    {
        #region Field        
        bool _disposed = false;     
        private RuleApplicationDef ruleAppDef;
        SafeHandle _handle = new SafeFileHandle(IntPtr.Zero, true);
        #endregion

        public TestResultCollection ExcuteTestSuite(TestSuiteFile testSuite)
        {            
            

            TestSuitePersistenceProvider testProvider = new ZipFileTestSuitePersistenceProvider(testSuite.FileName);

            TestSuiteDef suite = TestSuiteDef.LoadFrom(testProvider);

            ruleAppDef = RuleApplicationDef.Load($@"{testSuite.RuleAppFolderPath}\{suite.Settings.RuleAppName}.ruleappx");            
            suite.ActiveRuleApplicationDef = ruleAppDef;
          
            using (TestingSessionManager manager = new TestingSessionManager(new InProcessConnectionFactory()))
            {
                RegressionTestingSession sessionTest = new RegressionTestingSession(manager, suite);
                sessionTest.TestStart += SessionTest_TestStart;
                sessionTest.TestComplete += SessionTest_TestComplete;

                lock (InRuleDataServiceHelper.mockTestFileName)
                {
                    return sessionTest.ExecuteAllTests();
                }
            }                  
        }

        private void SessionTest_TestComplete(object sender, TestCompleteEventArgs args)
        {
            InRuleDataServiceHelper.mockTestFileName = string.Empty;           
        }

        private void SessionTest_TestStart(object sender, TestStartEventArgs args)
        {
            InRuleDataServiceHelper.mockTestFileName = $@"{ruleAppDef.Name}\{args.Test.DisplayName}";
            InruleExcutionHelper.BuildOverrideParamaters(ruleAppDef, args.Test, InRuleDataServiceHelper.mockTestFileName);
            InruleExcutionHelper.BuildOverrideAssertExpectValues(args.Test, InRuleDataServiceHelper.mockTestFileName);
            InruleExcutionHelper.LoadDataState(args.Test, InRuleDataServiceHelper.mockTestFileName);           

        } 
        
        
        
        #region Destructors

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                  _handle.Dispose();
                // Free any other managed objects here.
                ruleAppDef.Dispose();
            }

            // Free any unmanaged objects here.
            //
            _disposed = true;
        }

        #endregion
    }
}
