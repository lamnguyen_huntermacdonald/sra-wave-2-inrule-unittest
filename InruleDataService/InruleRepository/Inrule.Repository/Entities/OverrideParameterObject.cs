﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InRule.Repository.Regression;
using static InRule.Repository.Regression.DataStateOverrideDef;

namespace InruleUnitTest.Repository
{
    public class OverrideParameterObject
    {
        public OverrideParameterObject(string targerPath, string value, DataOverrideDefType dataOverrideDefType)
        {
            this.TargerPath = targerPath;
            this.Value = value;
            this.DataOverrideDefType = dataOverrideDefType;
        }
        public string TargerPath { get; set; }
        public string Value { get; set; }
        public DataOverrideDefType DataOverrideDefType { get; set; }
    }
}
