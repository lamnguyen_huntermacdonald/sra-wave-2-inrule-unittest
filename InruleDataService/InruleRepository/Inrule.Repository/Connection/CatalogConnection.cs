﻿using InRule.Repository.Client;
using InruleUnitTest.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InruleUnitTest.Repository
{
    public static class CatalogConnection
    {
        public static Uri Url { get; set; }
        public static string UserName { get; set; }
        public static string Password { get; set; }
        public static int Timeout { get; set; }

        static CatalogConnection()
        {
            IConfigurationRepository congig = new ConfigFileConfigurationRepository();
            Url = new Uri(congig.GetConfigurationValue<string>("InruleCatalogUrl"));
            UserName = congig.GetConfigurationValue<string>("InruleUserName");
            Password = congig.GetConfigurationValue<string>("Password");
            Timeout = congig.GetConfigurationValue<int>("Timeout");
        }
        public static RuleCatalogConnection CreateRuleCatalogConnection()
        {
            return new RuleCatalogConnection(Url, new TimeSpan(0, Timeout, 0), UserName, Password);
        }
    }
}
