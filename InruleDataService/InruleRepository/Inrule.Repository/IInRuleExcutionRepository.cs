﻿using InRule.Runtime.Testing.Regression;
using InruleUnitTest.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InruleUnitTest.Repository
{
    public interface IInRuleExcutionRepository
    {
        TestResultCollection ExcuteTestSuite(TestSuiteFile testSuite);
    }
}
