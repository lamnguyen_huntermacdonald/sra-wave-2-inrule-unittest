﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InRule.Repository;
using InRule.Repository.Regression;
using InRule.Runtime.Testing.Regression;
using InruleUnitTest.Infrastructure.Common;
using Newtonsoft.Json.Linq;
using static InRule.Repository.Regression.DataStateOverrideDef;

namespace InruleUnitTest.Repository
{
    public static class InruleExcutionHelper
    {
        private const string OverridePart = "OverrideParameters";
        private const string ExpectedResultPart = "ExpectedResults";
        private const string DataStatePart = "DataState";
        public static void BuildOverrideParamaters(RuleApplicationDef ruleAppDef, TestDef test, string filename)
        {            
            JToken jsonToken = ReadJsonFileHelper.GetDataFromJson(filename, OverridePart);

            var overrideData = ParseOverrideTestParameters(ruleAppDef, test, jsonToken);            

            if (overrideData != null && overrideData.Count > 0)
            {
                test.Overrides.Clear();
                foreach (var para in overrideData)
                {
                    var dataState = new DataStateOverrideDef();
                    dataState.DataOverrideType = para.DataOverrideDefType;
                    dataState.TargetPath = para.TargerPath;
                    dataState.Value = para.Value;
                    test.Overrides.Add(dataState);
                }
            }
        }

        public static void LoadDataState(TestDef test, string filename)
        {
            JToken jsonToken = ReadJsonFileHelper.GetDataFromJson(filename, DataStatePart);
            if (jsonToken != null && test.DataStates[0].DataState.DataStateType == DataStateType.EntityState)
            {
                string xml = JsonHelper.JsonToXml("{\"" + test.RootContextName + "\":" + jsonToken.ToString() + "}");
                test.DataStates[0].DataState.StateXml = xml;
            }
            
        }

        public static void BuildOverrideAssertExpectValues(TestDef test, string filename)
        {
            JToken jsonToken = ReadJsonFileHelper.GetDataFromJson(filename, ExpectedResultPart);
            var paraData = ParseOverrideExpectedResultParas(filename, ExpectedResultPart);
            if (paraData != null && paraData.Count > 0)
            {
                if (test.Assertions.Count > 0)
                {
                    foreach (var para in paraData)
                    {
                        var assert = test.Assertions.Where(x => x.TargetElementPath == para.Key).FirstOrDefault();
                        if (assert != null)
                        {
                            assert.ExpectedValue = para.Value;
                        }
                    }
                }
            }
        }
        private static IDictionary<string, string> ParseOverrideExpectedResultParas(string jsonFile, string jsonPath)
        {
            JToken jsonToken = ReadJsonFileHelper.GetDataFromJson(jsonFile, jsonPath);
            IDictionary<string, string> result = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(jsonToken.ToString()))
            {
                JArray array = JArray.Parse(jsonToken.ToString());
                foreach (JObject content in array.Children<JObject>())
                {
                    foreach (JProperty prop in content.Properties())
                    {
                        result.Add(prop.Name, prop.Value.ToString());
                    }
                }
            }
            return result;
        }

        private static IList<OverrideParameterObject> ParseOverrideTestParameters(RuleApplicationDef ruleAppDef, TestDef test, JToken jsonData)
        {
            IList<OverrideParameterObject> result = new List<OverrideParameterObject>();
            int index = 1;

            if (!string.IsNullOrEmpty(jsonData.ToString()))
            {
                JArray array = JArray.Parse(jsonData.ToString());
                foreach (JObject content in array.Children<JObject>())
                {
                    foreach (JProperty prop in content.Properties())
                    {
                        var element = ruleAppDef.Entities[test.RootContextName].Fields[prop.Name];
                        if (element != null)
                        {
                            if (element.FieldDefType == FieldDefType.Collection)
                            {
                                dynamic collection = JsonHelper.DeserializeObject<IList<ExpandoObject>>(prop.Value.ToString());
                                foreach (var expando in collection)
                                {
                                    var expandoDic = (IDictionary<string, object>)expando;
                                    result.Add(new OverrideParameterObject(prop.Name, string.Empty, DataOverrideDefType.AddCollectionMember));

                                    foreach (var item in expandoDic)
                                    {
                                        result.Add(new OverrideParameterObject($"{prop.Name}({index}).{item.Key}", item.Value.ToString(), DataOverrideDefType.SetValue));
                                    }
                                    index++;
                                }
                            }
                            else if (element.FieldDefType == FieldDefType.Field)
                            {
                                if (element.DataType == DataType.Entity ||
                                    element.DataType == DataType.Complex)
                                {
                                    dynamic expando = JsonHelper.DeserializeObject<ExpandoObject>(prop.Value.ToString());
                                    var expandoDic = (IDictionary<string, object>)expando;

                                    foreach (var item in expandoDic)
                                    {
                                        result.Add(new OverrideParameterObject($"{prop.Name}.{item.Key}", item.Value.ToString(), DataOverrideDefType.SetValue));
                                    }
                                }
                                else
                                {
                                    result.Add(new OverrideParameterObject(prop.Name, prop.Value.ToString(), DataOverrideDefType.SetValue));
                                }
                            }
                        }
                    }
                }
            }
            return result;

        }
    }
}
