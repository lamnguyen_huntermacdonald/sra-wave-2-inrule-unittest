﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InruleUnitTest.Domain;
using InruleUnitTest.InruleApplicationService;
using InruleUnitTest.Repository;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using System.IO;

namespace InRuleUnitTest
{
    [TestClass]
    public class SRARuleApplicationUnitTest
    {
        //[TestMethod]
        //public void Test()
        //{
        //    for(int i=0; i < 10;i++)
        //    {
        //        RuleSetDemoUnitTest();
        //    }
        //}
        [TestMethod]
        public void RuleSetDemoUnitTest()
        {
           
            IInruleExcutionService _inruleService = new InruleExcutionService(new InRuleExcutionRepository());
            var request = new TestSuiteRequest
            {
                TestSuite = new TestSuiteFile
                {
                    FileName = ConfigurationManager.AppSettings["FolderPathFileTest"] + @"\DemoTesting.testsuite",
                    RuleAppFolderPath = $@"{ConfigurationManager.AppSettings["FolderRuleApp"]}"
                }
            };
            var responese = _inruleService.ExcuteTestSuite(request);
            

            foreach (var testResult in responese.Result)
            {
                var filePath = $@"C:\LamNguyen\UnitTestCode\WriteLines2_{testResult.TestDef.DisplayName}.txt";                

                using (StreamWriter file = (File.Exists(filePath)) ? File.AppendText(filePath) : File.CreateText(filePath))               
                {                    
                    file.WriteLine("TestResult 1 execution duration: {0}.", testResult.Duration);

                    foreach (var assertResult in testResult.AssertionResults)
                    {

                        file.WriteLine("TestResult 1 {0}.", assertResult.Passed ? "passed" : "failed");

                        file.WriteLine("TestResult 1, Assertion 1 Expected Value: {0}.", assertResult.FormattedExpectedValue);

                        file.WriteLine("TestResult 1, Assertion 1 Actual Value: {0}.", assertResult.FormattedActualValue);

                        file.WriteLine("TestResult 1, Assertion 1 Target: {0}.", assertResult.Target);

                        file.WriteLine("TestResult 1, Assertion 1 Display Text: {0}.", assertResult.DisplayText);

                    }
                }
                Assert.AreEqual(testResult.Passed, true);
            }
        }


        //[TestMethod]
        //public void RuleExecutingRuleSet()
        //{

        //    IInruleExcutionService _inruleService = new InruleExcutionService(new InRuleExcutionRepository());
        //    var request = new TestSuiteRequest
        //    {
        //        TestSuite = new TestSuiteFile
        //        {
        //            FileName = ConfigurationManager.AppSettings["FolderPathFileTest"] + @"\DemoTesting.testsuite",
        //            RuleAppFileName = $@"{ConfigurationManager.AppSettings["FolderRuleApp"]}\SRARuleApplicationUnitTest1.ruleappx"
        //        }
        //    };
        //    var responese = _inruleService.ExcuteTestSuite(request);


        //    foreach (var testResult in responese.Result)
        //    {
        //        var filePath = $@"C:\LamNguyen\UnitTestCode\WriteLines2_{testResult.TestDef.DisplayName}.txt";

        //        using (StreamWriter file = (File.Exists(filePath)) ? File.AppendText(filePath) : File.CreateText(filePath))
        //        {
        //            file.WriteLine("TestResult 1 execution duration: {0}.", testResult.Duration);

        //            foreach (var assertResult in testResult.AssertionResults)
        //            {

        //                file.WriteLine("TestResult 1 {0}.", assertResult.Passed ? "passed" : "failed");

        //                file.WriteLine("TestResult 1, Assertion 1 Expected Value: {0}.", assertResult.FormattedExpectedValue);

        //                file.WriteLine("TestResult 1, Assertion 1 Actual Value: {0}.", assertResult.FormattedActualValue);

        //                file.WriteLine("TestResult 1, Assertion 1 Target: {0}.", assertResult.Target);

        //                file.WriteLine("TestResult 1, Assertion 1 Display Text: {0}.", assertResult.DisplayText);

        //            }
        //        }
        //        Assert.AreEqual(testResult.Passed, true);
        //    }
        //}

        //[TestMethod]
        //public void CheckCaseWorkRequired()
        //{

        //    IInruleExcutionService _inruleService = new InruleExcutionService(new InRuleExcutionRepository());
        //    var request = new TestSuiteRequest
        //    {
        //        TestSuite = new TestSuiteFile
        //        {
        //            FileName = ConfigurationManager.AppSettings["FolderPathFileTest"] + @"\TestLicense.testsuite",
        //            RuleAppFileName = $@"{ConfigurationManager.AppSettings["FolderRuleApp"]}\SRARuleApplicationUnitTest.ruleappx"
        //        }
        //    };
        //    var responese = _inruleService.ExcuteTestSuite(request);


        //    foreach (var testResult in responese.Result)
        //    {
        //        var filePath = $@"C:\LamNguyen\UnitTestCode\WriteLines2_{testResult.TestDef.DisplayName}.txt";

        //        using (StreamWriter file = (File.Exists(filePath)) ? File.AppendText(filePath) : File.CreateText(filePath))
        //        {
        //            file.WriteLine("TestResult 1 execution duration: {0}.", testResult.Duration);

        //            foreach (var assertResult in testResult.AssertionResults)
        //            {

        //                file.WriteLine("TestResult 1 {0}.", assertResult.Passed ? "passed" : "failed");

        //                file.WriteLine("TestResult 1, Assertion 1 Expected Value: {0}.", assertResult.FormattedExpectedValue);

        //                file.WriteLine("TestResult 1, Assertion 1 Actual Value: {0}.", assertResult.FormattedActualValue);

        //                file.WriteLine("TestResult 1, Assertion 1 Target: {0}.", assertResult.Target);

        //                file.WriteLine("TestResult 1, Assertion 1 Display Text: {0}.", assertResult.DisplayText);

        //            }
        //        }
        //        Assert.AreEqual(testResult.Passed, true);
        //    }
        //}

        //[TestMethod]
        //public void MulTiLoadUserLoadTest()
        //{

        //    IInruleExcutionService _inruleService = new InruleExcutionService(new InRuleExcutionRepository());
        //    var request = new TestSuiteRequest
        //    {
        //        TestSuite = new TestSuiteFile
        //        {
        //            FileName = ConfigurationManager.AppSettings["FolderPathFileTest"] + @"\TestMultiLoadUser.testsuite",
        //            RuleAppFileName = "SRARuleApplicationUnitTest"
        //        }
        //    };
        //    var responese = _inruleService.ExcuteTestSuite(request);


        //    foreach (var testResult in responese.Result)
        //    {
        //        var filePath = $@"C:\LamNguyen\UnitTestCode\WriteLines2_{testResult.TestDef.DisplayName}.txt";

        //        using (StreamWriter file = (File.Exists(filePath)) ? File.AppendText(filePath) : File.CreateText(filePath))
        //        {
        //            file.WriteLine("TestResult 1 execution duration: {0}.", testResult.Duration);

        //            foreach (var assertResult in testResult.AssertionResults)
        //            {

        //                file.WriteLine("TestResult 1 {0}.", assertResult.Passed ? "passed" : "failed");

        //                file.WriteLine("TestResult 1, Assertion 1 Expected Value: {0}.", assertResult.FormattedExpectedValue);

        //                file.WriteLine("TestResult 1, Assertion 1 Actual Value: {0}.", assertResult.FormattedActualValue);

        //                file.WriteLine("TestResult 1, Assertion 1 Target: {0}.", assertResult.Target);

        //                file.WriteLine("TestResult 1, Assertion 1 Display Text: {0}.", assertResult.DisplayText);

        //            }
        //        }
        //        Assert.AreEqual(testResult.Passed, true);
        //    }
        //}
    }
}
