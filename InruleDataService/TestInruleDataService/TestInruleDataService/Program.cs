﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InRuleDataService;
using InruleUnitTest.Domain;
using InruleUnitTest.InruleApplicationService;
using InruleUnitTest.Repository;

namespace TestInruleDataService
{
    class Program
    {
        static void Main(string[] args)
        {
            IInruleExcutionService _inruleService = new InruleExcutionService(new InRuleExcutionRepository());
            var request = new TestSuiteRequest
            {
                TestSuite = new TestSuiteFile
                {
                    FileName = @"C:\temp\SRARuleApplicationUnitTest_RuleSetDemoUnitTest.testsuite",
                    RuleAppFileName = "SRARuleApplicationUnitTest"
                }
            };
            var responese = _inruleService.ExcuteTestSuite(request);
          
            Console.ReadLine(); 
        }
    }
}
