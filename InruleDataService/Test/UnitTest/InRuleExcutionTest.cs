﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using InruleUnitTest.InruleApplicationService;

using InruleUnitTest.Domain;
using InruleUnitTest.Repository;

namespace InruleUnitTest.UnitTest
{
    public class InRuleExcutionTest
    {
        IInruleExcutionService _inruleService = new InruleExcutionService( new InRuleExcutionRepository());
        [Fact]
        public void PassingTest()
        {
            Assert.Equal(4, Add(2, 2));
        }

        [Fact]
        public void FailingTest()
        {
            Assert.Equal(4, Add(2, 2));
        }

        int Add(int x, int y)
        {
            return x + y;
        }

        // [Fact]
        //public void ExcuteTestSuiteTest()
        //{
        //    var request = new TestSuiteRequest { TestSuite = new TestSuiteFile {
        //        FileName = @"C:\temp\SRARuleApplicationUnitTest_RuleSetDemoUnitTest.testsuite",
        //        RuleAppName = "MasterHyprid" } };
        //    var responese = _inruleService.ExcuteTestSuite(request);

        //    foreach(var testResult in responese.Result)
        //    {
        //        foreach (var assertResult in testResult.AssertionResults)
        //        {
        //            Assert.True(testResult.Passed);
        //        }

        //    }
        //}
    }
}
