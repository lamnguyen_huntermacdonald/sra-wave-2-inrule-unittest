﻿using InruleUnitTest.Domain;
using InruleUnitTest.InruleApplicationService;
using InruleUnitTest.Repository;
using log4net;
using System;
using System.Configuration;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace InruleUnitTest.ConsoleApp
{
    class Program
    {
        #region Fields

        private static readonly ILog _log;

        #endregion

        #region Constructors

        static Program()
        {
            _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        }

        #endregion

        static void Main(string[] args)
        {
            Console.Clear();

           // Console.WriteLine("Input TestSuite file name: ");
            //string fileName = (Console.ReadLine());
            string fileName = "admqlts_6772_on_next.testsuite";
            _log.Info($"Start to executte TestSuite file {fileName}");
            try
            {
                IInruleExcutionService _inruleService = new InruleExcutionService(new InRuleExcutionRepository());
                var request = new TestSuiteRequest
                {
                    TestSuite = new TestSuiteFile
                    {
                        FileName = ConfigurationManager.AppSettings["FolderPathFileTest"] + $@"\{fileName}",
                        RuleAppFolderPath = $@"{ConfigurationManager.AppSettings["FolderRuleApp"]}"
                    }
                };
                var responese = _inruleService.ExcuteTestSuite(request);

                foreach (var testResult in responese.Result)
                {
                    if (string.IsNullOrEmpty(testResult.RuntimeErrorMessage))
                    {
                        _log.Info(testResult.TestDef.DisplayName);
                        foreach (var assertResult in testResult.AssertionResults)
                        {

                            _log.Info($"{assertResult.DisplayText} Passed : {assertResult.Passed}");

                            _log.Info($"{assertResult.DisplayText} Expected Value: {assertResult.FormattedExpectedValue}");

                            _log.Info($"{assertResult.DisplayText} Actual Value: {assertResult.FormattedActualValue}");

                            _log.Info($"{assertResult.DisplayText} Actual Value: Target: {assertResult.Target}");

                        }
                    }
                    else
                    {
                        _log.Info($"{testResult.TestDef.DisplayName} : {testResult.RuntimeErrorMessage}");
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
            }
        }
    }

}
