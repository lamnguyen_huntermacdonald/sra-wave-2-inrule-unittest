﻿//using InRuleDataService.Helpers;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace InRuleDataService
//{
//    public static class RestServiceDriver
//    {
//        #region Fields

//        static readonly HttpClientExecuter _clientExecuter;

//        #endregion

//        #region Constructors

//        static RestServiceDriver()
//        {
//            _clientExecuter = new HttpClientExecuter();
//        }

//        #endregion

//        #region Public Methods

//        /// <summary>
//        /// Execute REST API with GET method.
//        /// </summary>
//        /// <param name="apiPath">URL to API without root.</param>
//        /// <param name="query">String of URL parameters.</param>
//        /// <param name="paramSeparator">Separator between parameters.</param>
//        /// <returns></returns>
//        public static string Get(string apiPath, string query, char paramSeparator)
//        {
//            var urlBuilder = new StringBuilder(apiPath);
//            string[] parameters = query.Split(paramSeparator);
//            if (parameters.Length > 0)
//            {
//                urlBuilder.Append('?');
//                foreach (string parameter in parameters)
//                {
//                    urlBuilder.Append($"&{parameter}");
//                }
//            }

//            string url = urlBuilder.ToString();

//            var response = _clientExecuter.Execute(Method.GET, url, null, null);
//            return response;
//        }

//        /// <summary>
//        /// Execute REST API with GET method. Separator is ';'
//        /// </summary>
//        /// <param name="apiPath">URL to API without root.</param>
//        /// <param name="query">String of URL parameters.</param>
//        public static string Get(string apiPath, string query)
//        {
//            return Get(apiPath, query, ';');
//        }

//        #endregion
//    }
//}
