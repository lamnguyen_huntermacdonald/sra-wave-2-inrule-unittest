﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;

namespace InRuleDataService
{
    public static class DoxisServiceDriver
    {
        private static readonly HttpClient client;
        private static readonly string baseUrl;

        static DoxisServiceDriver()
        {
            client = new HttpClient();
            baseUrl = ConfigurationManager.AppSettings["Doxis_Url"];

            var token = ConfigurationManager.AppSettings["Doxis_Token"];

            if (String.IsNullOrEmpty(token) || String.IsNullOrEmpty(baseUrl))
                throw new ApplicationException("Missing Doxis setting Doxis_Url/Doxis_Token in InRule Config");

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("SharedAccessSignature", ConfigurationManager.AppSettings["Doxis_Token"]);

        }

        /// <summary>
        /// Get count of open matters for a give SRA ID
        /// </summary>
        /// <param name="sraId"></param>
        /// <param name="dateFrom"></param>
        /// <returns></returns>
        public static string GetOpenMatters(string sraId, string dateFrom)
        {
            return GetMatters(sraId, dateFrom, false, "open");
        }

        /// <summary>
        /// Call API Managment Endpoint to get matters held in Doxis 
        /// </summary>
        /// <param name="sraId"></param>
        /// <param name="dateFrom"></param>
        /// <param name="includeMatterDetails"></param>
        /// <param name="mattersToInclude"></param>
        /// <returns></returns>
        public static string GetMatters(string sraId, string dateFrom, bool includeMatterDetails, string mattersToInclude)
        {
            return client.GetStringAsync(
                new Uri($"{baseUrl}?dateFrom={dateFrom}&sraid={sraId}&includeMatterDetails={includeMatterDetails}&mattersToInclude={mattersToInclude}", UriKind.Absolute))
                .Result;
        }
    }
}
