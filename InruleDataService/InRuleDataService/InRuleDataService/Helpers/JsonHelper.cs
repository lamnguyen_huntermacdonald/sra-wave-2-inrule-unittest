﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace InRuleDataService
{
    public static class JsonHelper
    {
        public static JToken GetValueFromJsonPath(string jsondata, string jsonpath)
        {

            JToken jsonTokenreturn = null;
            try
            {
                JObject jobject = JObject.Parse(jsondata);
                jsonTokenreturn = jobject.SelectToken(jsonpath);


            }
            catch {

            }
            return jsonTokenreturn;
        }
        public static JToken GetValueFromJsonPathAndPathFile(string jsonfilepath, string jsonpath)
        {
            string jsondata = "";
            if (File.Exists(jsonfilepath))
            {
                jsondata = File.ReadAllText(jsonfilepath);
                return GetValueFromJsonPath(jsondata, jsonpath);
            }
            else
                return null;
          
        }

        public static string SerializeObject(object value)
        {
            try
            {
                return JsonConvert.SerializeObject(value);
            }
            catch
            {
                return "";
            }
        }
        public static T DeserializeObject<T>(string value)
        {
            try
            {
                return (T)JsonConvert.DeserializeObject<T>(value);
            }
            catch {
            }
            return default(T);
        }

    }
}
