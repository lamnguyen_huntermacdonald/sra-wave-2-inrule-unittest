﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace InRuleDataService.Helpers
{
    public static class WebHelper
    {
        public static string HtmlEncode(string encodeString)
        {
            return WebUtility.HtmlEncode(encodeString);
        }

        public static string HtmlDecode(string decodeString)
        {
            return WebUtility.HtmlDecode(decodeString);
        }

        public static string UrlEncode(string encodeString)
        {
            return WebUtility.UrlEncode(encodeString);
        }

        public static string UrlDecode(string decodeString)
        {
            return WebUtility.UrlDecode(decodeString);
        }
    }
}