﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.Configuration;
using InruleUnitTest.Infrastructure.Common;

namespace InRuleDataService.Helpers
{
    public static class InRuleDataServiceHelper
    {
        public static string mockTestFileName = "";
        private const string FakeDataPart = "FakeData";

        public static string GetMockData(string identityKey)
        {            
            var result = ReadJsonFileHelper.GetDataFromJson(mockTestFileName, $"{FakeDataPart}[?(@)].{identityKey}");
            return result;
        }
    }
}