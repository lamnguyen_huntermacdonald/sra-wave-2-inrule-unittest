﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel;
using System.Configuration;

namespace InRuleDataService.Helpers
{
    internal class HttpClientExecuter
    {
        Dictionary<string, string> _defaultHeaders = new Dictionary<string, string>();
        readonly Uri _baseUri;
        Method _defaultMethod = Method.GET;
        Action<string, string, string> _sendEmail;
        public bool IsBizTalk;
        public bool IsWorkflow;

        public HttpClientExecuter(Uri baseUri, Dictionary<string, string> defaultHeaders = null, Method defaultMethod = Method.GET)
        {
            _defaultHeaders = defaultHeaders ?? new Dictionary<string, string>();
            _baseUri = baseUri;
            //if (baseUri != null)
            //    _prefixUrl = baseUri.Scheme + "://" + baseUri.Host + (baseUri.Port != 80 ? $":{baseUri.Port}" : "");
            _defaultMethod = defaultMethod;
        }

        public HttpClientExecuter() : this(new Uri(ConfigurationManager.AppSettings["Gateway"]),
            new Dictionary<string, string>
            {
                { "Authorization", $"Basic {EncryptionHelper.EncodeBase64(ConfigurationManager.AppSettings["Gateway_Username"] + ":" + ConfigurationManager.AppSettings["Gateway_Password"])}" }
            })
        {

        }

        #region Methods 

        //public async Task<string> GetAsync(string prefixUrl, string apiPath, Dictionary<string, object> data, ErrorHandler handler, Dictionary<string, string> additionalHeaders = null, bool ignoreReferenceLoop = false, int timeOutInSecond = 100)
        //{
        //    _prefixUrl = prefixUrl;
        //    return await RunExecuteAsync(Method.GET, apiPath, data, handler, null, additionalHeaders, ignoreReferenceLoop, timeOutInSecond, ContentType.Json);
        //}
        public async Task<string> GetAsync(string apiPath, Dictionary<string, object> data, ErrorHandler handler, Dictionary<string, string> additionalHeaders = null, bool ignoreReferenceLoop = false, int timeOutInSecond = 100)
        {
            return await RunExecuteAsync(Method.GET, apiPath, data, handler, null, additionalHeaders, ignoreReferenceLoop, timeOutInSecond, ContentType.Json);
        }
        //public async Task<string> PostAsync(string prefixUrl, string apiPath, object data, ErrorHandler handler, Dictionary<string, string> additionalHeaders = null, bool ignoreReferenceLoop = false, int timeOutInSecond = 100)
        //{
        //    _prefixUrl = prefixUrl;
        //    return await PostAsync(apiPath, data, handler, additionalHeaders, ignoreReferenceLoop, timeOutInSecond);
        //}
        public async Task<string> PostAsync(string apiPath, object data, ErrorHandler handler, Dictionary<string, string> additionalHeaders = null, bool ignoreReferenceLoop = false, int timeOutInSecond = 100)
        {
            return await RunExecuteAsync(Method.POST, apiPath, data, handler, null, additionalHeaders, ignoreReferenceLoop, timeOutInSecond, ContentType.Json);
        }
        public async Task<string> PatchAsync(string apiPath, object data, ErrorHandler handler, Dictionary<string, string> additionalHeaders = null, bool ignoreReferenceLoop = false, int timeOutInSecond = 100)
        {
            return await RunExecuteAsync(Method.PATCH, apiPath, data, handler, null, additionalHeaders, ignoreReferenceLoop, timeOutInSecond, ContentType.Json);
        }
        public async Task<string> PutAsync(string apiPath, object data, ErrorHandler handler, Dictionary<string, string> additionalHeaders = null, bool ignoreReferenceLoop = false, int timeOutInSecond = 100)
        {
            return await RunExecuteAsync(Method.PUT, apiPath, data, handler, null, additionalHeaders, ignoreReferenceLoop, timeOutInSecond, ContentType.Json);
        }
        public async Task<string> PutAsync(string apiPath, object data, ErrorHandler handler, ContentType contentType, Dictionary<string, string> additionalHeaders = null, bool ignoreReferenceLoop = false, int timeOutInSecond = 100)
        {
            return await RunExecuteAsync(Method.PUT, apiPath, data, handler, null, additionalHeaders, ignoreReferenceLoop, timeOutInSecond, contentType);
        }
        public async Task<string> DeleteAsync(string apiPath, ErrorHandler handler, Dictionary<string, string> additionalHeaders = null, bool ignoreReferenceLoop = false, int timeOutInSecond = 100)
        {
            return await RunExecuteAsync(Method.DELETE, apiPath, null, handler, null, additionalHeaders, ignoreReferenceLoop, timeOutInSecond, ContentType.Json);
        }

        #endregion

        #region Executer

        public string Execute(Method method, string apiPath, object data, ErrorHandler handler, Dictionary<string, string> additionalHeaders = null, ContentType contentType = ContentType.Json, bool ignoreReferenceLoop = false, int timeOutInSecond = 100, bool seializeToJson = true)
        {
            return RunExecute(method, apiPath, data, handler, null, additionalHeaders, ignoreReferenceLoop, timeOutInSecond, contentType, seializeToJson);
        }        
        public async Task<string> ExecuteAsync(Method method, string apiPath, object data, ErrorHandler handler, Dictionary<string, string> additionalHeaders = null, ContentType contentType = ContentType.Json, bool ignoreReferenceLoop = false, int timeOutInSecond = 100)
        {
            return await RunExecuteAsync(method, apiPath, data, handler, null, additionalHeaders, ignoreReferenceLoop, timeOutInSecond, contentType);
        }
        public async Task<string> UploadAsync(string apiPath, object data, List<UploadFileDto> files, ErrorHandler handler, Dictionary<string, string> additionalHeaders = null, int timeOutInSecond = 300)
        {
            return await RunExecuteAsync(Method.POST, apiPath, data, handler, files, additionalHeaders, false, timeOutInSecond, ContentType.Json);
        }

        public async Task<byte[]> DownloadAsync(string apiPath, object data, ErrorHandler handler, Dictionary<string, string> additionalHeaders = null, int timeOutInSecond = 300)
        {
            //await AliveChecker(apiPath);
            byte[] result = null;
            string url = apiPath;
            try
            {
                using (HttpClient client = PrepareHttpClient(additionalHeaders))
                {
                    var values = PrepareExecute(ref url, data, Method.GET, false);
                    //var content = new StringContent(values);
                    HttpResponseMessage response = await client.GetAsync(url);
                    result = await response.Content.ReadAsByteArrayAsync();
                }
            }
            catch (WebException wexc)
            {
                CatchWebException(wexc, handler, url);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        string RunExecute(Method method, string apiPath, object data, ErrorHandler handler, List<UploadFileDto> files, Dictionary<string, string> additionalHeaders, bool ignoreReferenceLoop, int timeOutInSecond, ContentType contentType, bool seializeToJson = true)
        {
            var result = Task.Run(async () => await RunExecuteAsync(method, apiPath, data, handler, files, additionalHeaders, ignoreReferenceLoop, timeOutInSecond, contentType, null, seializeToJson));
            result.Wait();
            return result.Result;
        }

        async Task<string> RunExecuteAsync(Method method, string apiPath, object data, ErrorHandler handler, List<UploadFileDto> files, Dictionary<string, string> additionalHeaders, bool ignoreReferenceLoop, int timeOutInSecond, ContentType contentType, string prefix = null, bool seializeToJson = true)
        {
            //await AliveChecker(apiPath);
            string result = string.Empty;
            string url = apiPath;
            try
            {
                using (HttpClient client = PrepareHttpClient(additionalHeaders))
                {
                    HttpResponseMessage response = null;
                    HttpContent content;
                    if (files != null)
                    {
                        content = PrepareUploadFiles(files, data);
                    }
                    else
                    {
                        content = PrepareExecute(ref url, data, method, ignoreReferenceLoop, contentType, seializeToJson);
                        //content = new StringContent(values, Encoding.UTF8, contentType.Description());
                    }

                    switch (method)
                    {
                        case Method.GET: response = await client.GetAsync(url); break;
                        case Method.POST: response = await client.PostAsync(url, content); break;
                        case Method.PUT: response = await client.PutAsync(url, content); break;
                        case Method.DELETE: response = await client.DeleteAsync(url); break;
                        default:
                            response = await client.SendAsync(new HttpRequestMessage(new HttpMethod(method.ToString()), url) { Content = content });
                            break;
                    }

                    if (response != null)
                    {
                        result = await response.Content.ReadAsStringAsync();
                        if (!response.IsSuccessStatusCode)
                            HandleErrorResponse(response, handler, result);
                    }
                }
            }
            catch (TaskCanceledException ex)
            {
                // Check ex.CancellationToken.IsCancellationRequested here.
                // If false, it's pretty safe to assume it was a timeout.
                if (!ex.CancellationToken.IsCancellationRequested)
                    throw new Exception($"Connector Timeout occurs at {url}");
                //else
                //    throw new Exception($"Task Cancel of Connector with {url} has exception: " + StaticHelper.CursiveExceptionMessage(ex));
            }
            catch (WebException wexc)
            {
                CatchWebException(wexc, handler, url);
            }
            catch (Exception ex)
            {
                //_helper.WriteLogError($"Connector with {url} has exception: ", ex);                
                throw new Exception($"Connector with {url} has exception: " + StaticHelper.CursiveExceptionMessage(ex));
            }
            return result;
        }
        #endregion

        #region Private functions 

        private HttpClient PrepareHttpClient(Dictionary<string, string> additionalHeaders)
        {
            var handler = new CustomHandler();
            HttpClient client = new HttpClient(handler);
            //client.BaseAddress = _baseUri;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            foreach (var item in _defaultHeaders)
                client.DefaultRequestHeaders.Add(item.Key, item.Value);
            if (additionalHeaders != null)
                foreach (var item in additionalHeaders)
                    client.DefaultRequestHeaders.Add(item.Key, item.Value);

            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            return client;
        }

        HttpContent PrepareExecute(ref string url, object data, Method method, bool ignoreReferenceLoop, ContentType contentType = ContentType.Json, bool seializeToJson = true)
        {
            HttpContent content = new StringContent("");
            if (this._baseUri != null && string.IsNullOrWhiteSpace(this._baseUri.AbsoluteUri) == false && url.StartsWith("https://") == false && url.StartsWith("http://") == false)
                url = StaticHelper.UrlPathCombine(_baseUri.AbsoluteUri, url);

            if (data == null || data.GetType().Name == "Byte[]")
                return content;

            if (method == Method.GET && data is Dictionary<string, object>)
            {
                string getParamString = StaticHelper.DictToString(data as Dictionary<string, object>);
                url = StaticHelper.UrlParamCombine(url, getParamString);
            }
            else if (contentType == ContentType.Json)
            {
                string jsonRequest = string.Empty;
                if (seializeToJson)
                {
                    if (!IsBizTalk)
                    {
                        jsonRequest = ignoreReferenceLoop ? JsonConvert.SerializeObject(data, Formatting.Indented,
                                                                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, /*Converters = new List<JsonConverter>() { new CustomDateTimeConverter() }*/ })
                                                            //: JsonConvert.SerializeObject(data, new CustomDateTimeConverter());
                                                            : JsonConvert.SerializeObject(data);
                    }
                    else
                    {
                        jsonRequest = ignoreReferenceLoop ? JsonConvert.SerializeObject(data, Formatting.Indented,
                                                            new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore })
                                                        //: JsonConvert.SerializeObject(data, new BizTalkDateTimeConverter());
                                                        : JsonConvert.SerializeObject(data);
                    }
                }
                else
                {

                    jsonRequest = data.ToString();

                }
                content = new StringContent(jsonRequest, Encoding.UTF8, contentType.Description());
            }
            else if (contentType == ContentType.FormEncoded)
            {
                var reqContent = new Dictionary<string, string>();
                if (data is Dictionary<string, object>)
                    foreach (var item in data as Dictionary<string, object>)
                        reqContent.Add(item.Key, item.Value.ToString());
                else if (data is Dictionary<string, string>)
                    reqContent = data as Dictionary<string, string>;
                else
                    foreach (var item in data.GetType().GetProperties())
                        reqContent.Add(item.Name, item.GetValue(data, null).ToString());

                content = new FormUrlEncodedContent(reqContent);
            }
            else if (contentType == ContentType.Xml)
            {
                content = new StringContent(data.ToString());
            }

            return content;
        }

        MultipartFormDataContent PrepareUploadFiles(List<UploadFileDto> files, object data)
        {
            var formContent = new MultipartFormDataContent("---" + DateTime.Now.Ticks);
            var values = new Dictionary<string, object>();
            if (data != null)
                foreach (var p in data.GetType().GetTypeInfo().GetProperties())
                {
                    var value = p.GetValue(data);
                    if (value == null)
                        continue;
                    if (p.PropertyType.IsArray || p.PropertyType.GetTypeInfo().IsGenericType)
                    {
                        int counter = 0;
                        foreach (var subItem in (IEnumerable)value)
                        {
                            values[p.Name] = subItem.ToString();
                            counter++;
                        }
                    }
                    else
                        values[p.Name] = value.ToString();
                }

            foreach (var item in values)
                formContent.Add(new StringContent(JsonConvert.SerializeObject(item.Value)), item.Key);

            foreach (var file in files)
            {
                var fileContent = new StreamContent(file.Stream);
                fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                {
                    Name = $"\"{file.Name}\"",
                    FileName = $"\"{file.Filename}\""
                };
                fileContent.Headers.ContentType = new MediaTypeHeaderValue(file.ContentType);
            }
            return formContent;
        }

        public void HandleErrorResponse(HttpResponseMessage response, ErrorHandler handler, string error)
        {
            if (handler != null)
            {
                if (response.StatusCode == HttpStatusCode.BadRequest && handler.E400 != null)
                    handler.E400.Invoke(error);
                else if (response.StatusCode == HttpStatusCode.Unauthorized && handler.E401 != null)
                    handler.E401.Invoke(error);
                else if (response.StatusCode == HttpStatusCode.InternalServerError && handler.E500 != null)
                    handler.E500.Invoke(error);
                else if (handler.AnyError != null)
                    handler.AnyError.Invoke(error, response.StatusCode);
            }
            else if (response.StatusCode != HttpStatusCode.Unauthorized || response.StatusCode != HttpStatusCode.Forbidden)
            {
                throw new Exception(error);
            }
        }

        string CatchWebException(WebException wexc, ErrorHandler handler, string url)
        {
            HttpStatusCode code = HttpStatusCode.BadRequest;
            string error = CatchWebRequestException(wexc, rep => { code = rep.StatusCode; });
            if (handler != null && wexc.Response != null)
            {
                if (code == HttpStatusCode.BadRequest && handler.E400 != null)
                    handler.E400.Invoke(error);
                else if (code == HttpStatusCode.Unauthorized && handler.E401 != null)
                    handler.E401.Invoke(error);
                else if (code == HttpStatusCode.InternalServerError && handler.E500 != null)
                    handler.E500.Invoke(error);
                else if (handler.AnyError != null)
                    handler.AnyError.Invoke(error, code);
            }
            else if (code != HttpStatusCode.Unauthorized || code != HttpStatusCode.Forbidden)
            {
                throw new Exception(error);
            }
            return error;
        }
        public string CatchWebRequestException(WebException wexc, Action<HttpWebResponse> acquire = null)
        {
            string msg = wexc.Message ?? "Response is null!.";
            if (wexc != null && wexc.Response != null)
            {
                HttpWebResponse httpResponse = (HttpWebResponse)wexc.Response;
                acquire?.Invoke(httpResponse);
                using (Stream data = wexc.Response.GetResponseStream())
                using (var reader = new StreamReader(data))
                {
                    msg = reader.ReadToEnd();
                }
            }

            else if (wexc.InnerException != null)
                msg = wexc.InnerException.Message;

            return msg;
        }


        #endregion


    }

    internal class CustomHandler : DelegatingHandler
    {
        public CustomHandler() : base()
        {
            InnerHandler = new HttpClientHandler();
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            //request.Headers.Authorization = new AuthenticationHeaderValue("OAuth", authHeader);


            return base.SendAsync(request, cancellationToken);
        }
    }

    public enum Method
    {
        NONE,
        POST,
        GET,
        PUT,
        DELETE,
        GETSREAM,
        POSTMULTIPART,
        PATCH
    }

    public enum ContentType
    {
        [Description("application/json")]
        Json,
        [Description("application/xml")]
        Xml,
        [Description("application/x-www-form-urlencoded;odata.metadata=none")]
        FormEncoded,
        [Description("odata.metadata=none")]
        Odata
    }

    public class ErrorHandler
    {
        public Action<string> E400 { get; set; }
        public Action<string> E500 { get; set; }
        public Action<string> E401 { get; set; }
        public Action<string, System.Net.HttpStatusCode> AnyError { get; set; }
    }

    public class UploadFileDto
    {
        public UploadFileDto()
        {
            ContentType = "application/octet-stream";
        }
        public string Name { get; set; }
        public string Filename { get; set; }
        public string ContentType { get; set; }
        public Stream Stream { get; set; }
    }
}
