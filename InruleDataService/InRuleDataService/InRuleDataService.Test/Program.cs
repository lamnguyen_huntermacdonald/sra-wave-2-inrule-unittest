﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InRuleDataService.Helpers;
using InRuleService;

namespace InRuleDataService.Test
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            TestMultipleFetch.Execute();
            return;

            Stopwatch stopwatch = Stopwatch.StartNew();

            //var response = DataServiceDriver.GetByApiQuery("sra_ref_applicationtype", "sra_code eq 'PC'");
            //var response = DataServiceDriver.GetByFetchXML(FetchSimple);
            var testEncodeHtml = WebHelper.UrlEncode("ASSO2 & lele company test");
            var responseDoc = DataServiceDriver.GetSraDocumentsByApplicationId("fd8230d8-4fe7-4ab7-895d-779857470147", true);

            // var response = DataServiceDriver.GetByFetchXML(FetchSimple);

            string body = @"{
                                'ApplicationType': 6,
                                'ReducedFeeType': 0,
                                'AppliedDate': '2018-01-30T00:00:00',
                                'HasReg3': false,
                                'Reg3StartDate': '2018-01-08T00:00:00',
                               'LasPCApplicationStartDate':'2018-01-20T00:00:00',
                                'CPS':false,
                                'PaymentEvent':1
                            }";

            var response = InRuleServiceDriver.ExecuteRuleSet("FeeEngine", "IndividualPCREApplicationFeeService", "IndividualPCREApplicationFeeService_CalculateFeesByPaymentEvent", body);

            stopwatch.Stop();
            Console.WriteLine("Execution time (milisecond): {0:N0}", stopwatch.ElapsedMilliseconds);
            Console.WriteLine("-----------------------------------------------------------------------");

            //Console.WriteLine(response);
            Console.WriteLine(responseDoc);

            Console.Read();
        }

        private const string FetchSimple = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true' aggregate='true'>
	        <entity name='sra_organisationsolicitor'>
		        <attribute name='sra_isoptedin' alias ='OptedIn' groupby='true'/>
		        <attribute name='sra_code' alias ='Code' groupby='true'/>
		        <link-entity name='contact' from='contactid' to='sra_contact' alias ='Contact'>
			        <attribute name='sra_sraid' alias='SraId' groupby='true'/>
			        <attribute name='sra_workforthecrownprosecutionservice' alias='CPS' groupby='true'/>
			        <attribute name='fullname' alias ='FullName' groupby='true'/>
			        <attribute name='contactid' alias ='ContactId' groupby='true'/>
			        <attribute name='mobilephone' alias ='MobilePhone' groupby='true'/>
			        <link-entity name='sra_ref_salutation' from='sra_ref_salutationid' to='sra_title' alias ='TitleEntity'  link-type='outer'>
				        <attribute name='sra_label' alias ='Title' groupby='true'/>
			        </link-entity>
			        <link-entity name='sra_attribute' from='sra_contact' to='contactid' alias ='Person'>
				        <link-entity name='sra_ref_attributetype' from='sra_ref_attributetypeid' to='sra_attributetype' alias ='PTAttainment'>
					        <attribute name='sra_code' alias ='PCREType' groupby='true'/>
					        <filter type='and'>
						        <condition attribute='sra_code' operator='eq' value='REL' />
					        </filter>
				        </link-entity>
				        <filter type='and'>
					        <condition attribute='statuscode' operator='eq' value='165820000' />
				        </filter>
			        </link-entity>
		        </link-entity>
		        <filter type='and'>
			        <condition attribute='sra_isoptedin' operator='eq' value='165820000' />
			        <condition attribute='sra_organisation' operator='eq' value='D05C2308-0CB3-E711-8106-70106FAAEAD1' />
		        </filter>
	        </entity>
        </fetch>";
    }
}