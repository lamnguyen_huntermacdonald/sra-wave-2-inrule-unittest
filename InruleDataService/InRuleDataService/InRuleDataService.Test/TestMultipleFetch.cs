﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InRuleDataService.Test
{
    class TestMultipleFetch
    {        
        public static void Execute()
        {
            string input = @"{
                `accountList`: { 
	                `entity`: `account`,
	                `query`:`<fetch count='5' version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
					                <entity name='account'>
						                <attribute name='accountid'/> 
						                <attribute name='name'/> 
					                </entity>
				                </fetch>`
                },
                `contactList`: {
	                `entity`: `contact`,
	                `query`: `<fetch count='3' version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
					                <entity name='contact'>
						                <attribute name='contactid'/> 
						                <attribute name='emailaddress1'/> 
					                </entity>
				                </fetch>`
                }
            }";
            DataServiceDriver.GetByMultiFetchXML(input, false);
        }
    }
}
